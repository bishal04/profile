import React, { Component } from "react";
import "./App.css";
import profile from "./bishal.jpg";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, Row, Col, Card, ProgressBar } from "react-bootstrap";

class App extends Component {
  state = {
    img_url: profile,
    name: "Bishal Danuwar",
    email: "christbishal24@gmail.com",
    contact: "9866889997",
    address: "Lainchour, Kathmandu",
    gitlabUsername: "bishal24",
    instausername: "bishal.danuwar",
    linkedinusername: "Bishal Danuwar",
    project: [
      { name: "Counter", progess: 60 },
      { name: "Contact-app", progess: 70 },
      { name: "Todolist", progess: 50 },
      { name: "Weather-app", progess: 80 },
      { name: "Form part-2", progess:50 }
    ]
  };
  render() {
    return (
      <Container fluid>
        <Row>
          <Col style={{ textAlign: "center" }}>
            <Card>
              <Card.Header><strong>PROFILE</strong></Card.Header>
              <Card.Img Variant="top" src={this.state.img_url} />
              <Card.Body>
                <b>GitLab Username :</b>{" "}
                <Card.Link href="https://gitlab.com/bishal04">
                  <i>{this.state.gitlabUsername}</i>
                </Card.Link>
              </Card.Body>
            </Card>
          </Col>
          <Col sm={4} style={{ textAlign: "left" }}>
            <Card>
              <Card.Header ><strong>DETAILS</strong></Card.Header>
              <Card.Body>
                <Card.Text><b>Name :</b> {this.state.name}</Card.Text>
                <Card.Text><b>Email :</b> {this.state.email}</Card.Text>
                <Card.Text><b>Contact :</b> {this.state.contact}</Card.Text>
                <Card.Text><b>Address :</b> {this.state.address}</Card.Text>
                <Card.Text><b>Instagram Username :</b>
                <Card.Link href="https://www.instagram.com/bishal.danuwar/"> <i>{this.state.instausername}</i></Card.Link>
                </Card.Text>
                <Card.Text><b>Linkedin Username :</b>
                <Card.Link href="https://www.linkedin.com/in/bishal-danuwar-139a1a137/"> <i>{this.state.linkedinusername}</i></Card.Link>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card>
              {" "}
              <Card.Header><strong>PROJECTS</strong></Card.Header>
              <Card.Body>
                {this.state.project.map(value => (
                  <Card.Text>
                    <b>{value.name}</b>
                    <ProgressBar
                      variant="success"
                      label={value.progess + "%"}
                      now={value.progess}
                    />
                  </Card.Text>
                ))}
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default App;

